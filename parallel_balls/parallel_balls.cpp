/*
*	Ohrada s paralelnimi balonky 
*	OSCR - Operacni systemy realneho casu - VLA0054
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

#define height 32				// height of playground
#define width 64				// width of playground
#define maxSleep 500			// max sleep time - minimum speed
#define minSleep 200			// min sleep time - maximum speed
#define defaultThreadCount 5	// default count of balls

struct Ball
{
	int xPos;
	int yPos;
	int horDirection;
	int verDirection;
	int refreshTime;
};

// function declaration
void* ballProcessing(void *arg);
void initBall(Ball*);
void moveBall(Ball*);
void detectCollision(Ball*);
void drawBall(Ball*);
void clearBall(Ball*);
int getIndex(int, int);
void setCursorPos(int, int);
void drawFrame();
void clearScreen();
void keyboardInterruptHandler(int);

int playgroundMatrix[width * height] = {0};
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/*
*	compile QNX: qcc -o balls parallel_balls.cpp
*	compile Linux: gcc -o balls parallel_balls.cpp -lpthread
*	run: ./balls [balls_count]
*	stop: keyboard interrupt CTRL+C
*/
int main(int argc, char *argv[]) 
{
	signal(SIGINT, keyboardInterruptHandler); // handle keyboard interrupt with ctrl+c

	int threadCount = defaultThreadCount;
	if (argc > 1)
	{
		// scan console argument - thread count
		threadCount = (sscanf(argv[1], "%d", &threadCount) == 0) ? defaultThreadCount : threadCount;
	}
	srand((int) time(NULL)); // init random generator

	drawFrame(); // draw balls playground frame

	pthread_barrier_t barrier;
	pthread_barrier_init(&barrier, NULL, threadCount + 1);

	pthread_attr_t attr;
	pthread_attr_init( &attr );
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	pthread_t threads[threadCount];  // necessary for compilation on linux
	// start threads
	for (int i = 0; i < threadCount; i++)
	{
		pthread_create(&threads[i], &attr, &ballProcessing, NULL );
	}

	pthread_barrier_wait(&barrier); // wait for all threads
	
	setCursorPos(0, height + 2); // set cursof for prompt
	printf("End of simulation\n");

    return 0;
}

void* ballProcessing(void *arg)
{
	struct Ball ball;
	initBall(&ball);
	drawBall(&ball);	

	while (true)
	{
		detectCollision(&ball);
		clearBall(&ball);
		moveBall(&ball);
		drawBall(&ball);
		// printf("Thread:%d, x:%d, y:%d, hor:%d, ver:%d, speed:%d\n", pthread_self(), ball.xPos, ball.yPos, ball.horDirection, ball.verDirection, ball.refreshTime);

		#ifdef __QNX__
			delay(ball.refreshTime);
		#elif __linux__
			usleep(ball.refreshTime * 1000);
		#endif
	}

	return(0);
}

void initBall(Ball *ball)
{
	ball->xPos = (rand() % (width - 1)) + 1;
	ball->yPos = (rand() % (height - 1)) + 1;
	ball->horDirection = (rand() % 3) - 1;
	ball->verDirection = (rand() % 3) - 1;
	// prevent situation when verDirection and horDirection are both 0 -> ball does not move
	ball->verDirection = (ball->horDirection == 0 && ball->verDirection == 0) ? ((-1) + ((rand() % 2) * 2)) : ball->verDirection; // generate -1 or 1
	ball->refreshTime = (rand() % (maxSleep - minSleep)) + minSleep;
}

void moveBall(Ball *ball)
{
	ball->xPos += ball->horDirection;
	ball->yPos += ball->verDirection;
}

void detectCollision(Ball *ball)
{
	int nextXPos = ball->xPos + ball->horDirection;
	int nextYPos = ball->yPos + ball->verDirection;
	if (nextXPos == 0 || nextXPos == width)
	{
		ball->horDirection *= (-1);
	}
	if (nextYPos == 0 || nextYPos == height)
	{
		ball->verDirection *= (-1);
	}
}

void drawBall(Ball *ball)
{
	pthread_mutex_lock(&mutex);

	playgroundMatrix[getIndex(ball->xPos, ball->yPos)] += 1; // increment positional matrix
	setCursorPos(ball->xPos, ball->yPos);
	printf("*\n");
	setCursorPos(0, height + 1);

	pthread_mutex_unlock(&mutex);	
}

void clearBall(Ball *ball)
{
	pthread_mutex_lock(&mutex);
	
	playgroundMatrix[getIndex(ball->xPos, ball->yPos)] -= 1; // decrement positional matrix
	if (playgroundMatrix[getIndex(ball->xPos, ball->yPos)] == 0) // overlapping detection
	{
		setCursorPos(ball->xPos, ball->yPos);
		printf(" \n");
		setCursorPos(0, height + 1);	
	}

	pthread_mutex_unlock(&mutex);		
}

// get index in playground positional matrix
int getIndex(int x, int y)
{
	return (y * width) + x;
}

void drawFrame() 
{
	clearScreen();
	// vertical borders
	for (int i = 0; i <= width ; i++)
	{
		setCursorPos(i, 0);
		printf("#\n");
		setCursorPos(i, height);
		printf("#\n");
			
	}
	// horizontal borders
	for (int i = 0; i <= height; i++)
	{
		setCursorPos(0, i);
		printf("#\n");			
		setCursorPos(width, i);
		printf("#\n");					
	}
}

void setCursorPos(int xPos, int yPos)
{
	printf("\033[%d;%dH", yPos + 1, xPos + 1);	
}

void clearScreen()
{
	printf("\033[H\033[J");
}

void keyboardInterruptHandler(int signal)
{
	setCursorPos(0, height + 2); // set cursof for prompt
	printf("End of simulation\n");
	exit(EXIT_SUCCESS);
}
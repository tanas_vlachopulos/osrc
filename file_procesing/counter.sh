#!/bin/sh

size=1000000
threads=4
file=""
output=""

if [ -z $1 ]
then
	echo Start with default parameters
fi

while [ "$1" != "" ]
do
	case $1 in
		-s | --size )		shift
							size=$1
							;;
		-t | --threads )	shift
							threads=$1
							;;
		-f | --file )		shift
							file=$1
							;;
		-o | --output )		shift
							output=$1
							;;
		-h | --help )		echo "counter.sh [-s size_of_sample_file] [-t threads_count] [-f custom_data_file]"
							exit
							;;
	esac
	shift
done

# check file name
if [ ! -f $file ]
then
	echo file $file does not exist
	exit
fi

# check size argument
if ! echo $size | egrep -q '^[0-9]+$'
then
	echo Size must be a number
	exit
fi

if [ $size -le 0 ]
then
	echo Invalid size argument
	exit
fi

# check threads argument
if ! echo $threads | egrep -q '^[0-9]+$'
then
	echo Thread count must be a number
	exit
fi

if [ $threads -le 0 ]
then
	echo Invalid threads count
	exit
fi

start_time=$(date +%s%N)

echo Generate a file size of $size B
./generate_file $size

echo "Start counting char frequency in $threads threads"
if [ -z $output ]
then
	./count_char_dummy $threads $file
else
	echo "Printing stats to $output"
	./count_char_dummy $threads $file > $output
fi

end_time=$(date +%s%N)
echo "Elapsed time: $(( ($end_time - $start_time) / 1000000 )) ms"
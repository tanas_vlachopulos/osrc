#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <map>

#define ALFABET_SIZE 62
#define DEFAULT_THREAD_COUNT 1
#define DEFAULT_FILE_PATH "data.txt"

struct thread_args {
	long int arraySize;
	int stepSize;
	int threadId;
	char *charArray;
};

const char alfabet[ALFABET_SIZE + 1] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
pthread_barrier_t barrier;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
std::map<char,int> globalStats;

long int loadFile(const char*, char**);
void* countChars(void*);
void sumStats(std::map<char,int>);
void printStats(std::map<char,int>);

int main(int argc, char *argv[]) {
	int threadCount = DEFAULT_THREAD_COUNT;
	if (argc > 1) // read thread count from params
		threadCount = (sscanf(argv[1], "%d", &threadCount) == 0) ? DEFAULT_THREAD_COUNT : threadCount;

	for (int c = 0; c < ALFABET_SIZE; c++) // clear map
			globalStats[alfabet[c]] = 0;

	char *charArray = NULL;
	long int size = (argc > 2) ? loadFile(argv[2], &charArray) : loadFile(DEFAULT_FILE_PATH, &charArray); // load custom or default source file

	pthread_barrier_init(&barrier, NULL, threadCount + 1);
	pthread_t threads[threadCount];  // necessary for compilation on linux
	
	for (int i = 0; i < threadCount; i++) { 
		struct thread_args *parameters = (struct thread_args*) malloc(sizeof(struct thread_args)); // MEM -
		parameters->arraySize = size; // data array size
		parameters->stepSize = threadCount;
		parameters->threadId = i; 
		parameters->charArray = charArray;
		pthread_create(&threads[i], NULL, countChars, parameters ); // start threads
	}

	pthread_barrier_wait(&barrier); // wait for all threads

	for (int c = 0; c < ALFABET_SIZE; c++)  // print stats
		printf("%c ... %d\n", alfabet[c], globalStats[alfabet[c]]);
	printf("\n");

	if (charArray != NULL)
		free(charArray); // MEM +

	return EXIT_SUCCESS;
}

void* countChars(void *args) {
	if (args != NULL) {
		struct thread_args *param = (struct thread_args *)args;
		std::map<char,int> localStats;

		for (int c = 0; c < ALFABET_SIZE; c++) // clear map
			localStats[alfabet[c]] = 0;
		
		for (int i = param->threadId; i < param->arraySize; i += param->stepSize)
			localStats[param->charArray[i]] += 1;

		sumStats(localStats);
		free(param); // MEM + 
	}
	pthread_barrier_wait(&barrier);
}

long int loadFile(const char *filePath, char **charArray) {
	FILE *fd;
	fd = fopen(filePath, "r");

	fseek(fd, 0L, SEEK_END); // jump to end of file
	long int fileSize = ftell(fd); // get file position
	rewind(fd); // jump to begin of file

	*charArray = (char*) malloc(sizeof(char) * fileSize); // MEM -
	fread(*charArray, 1, fileSize, fd); // copy block of mem from file 

	fclose(fd);
	return fileSize;
}

void sumStats(std::map<char,int> result) {
	pthread_mutex_lock(&mutex);
	for (int c = 0; c < ALFABET_SIZE; c++) // clear map
			globalStats[alfabet[c]] += result[alfabet[c]]; // add local thread statistics to global statistics
	pthread_mutex_unlock(&mutex);
}
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	#ifdef __QNX__
		printf("Hello QNX\n");
	#elif __linux__
		printf("Hello linux\n");
	#else
		printf("Hello something other");
	#endif
	
	return 0;
}
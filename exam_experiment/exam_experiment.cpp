#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

#define width 64
#define height 32
#define verticalCenter 16
#define platformRadius 2 // radius of platform
#define platformOffset 2 // distance between border and platform
#define sleepTime 200

struct Ball
{
	int xPos;
	int yPos;
	int horDir;
	int verDir;
};

struct Platform
{
	int xPos;
	int yPos;
	int radius;
};

void moveBall();
int getIndex(int, int);
void drawFrame();
void setCursorPos(int, int);
void clearScreen();
void initPlayground();
int detectCollision();
void drawBall();
void clearBall();
void *ballProgress(void*);
void drawPlatform(Platform*);
void clearPlatform(Platform*);

int playground[width * height] = {0};
struct Ball ball;
struct Platform platformL, platformR;

int main()
{
	srand((int) time(NULL));
	initPlayground();
	drawFrame();
	drawBall();
	drawPlatform(&platformL);
	drawPlatform(&platformR);

	pthread_attr_t attr;
	pthread_attr_init( &attr );
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	pthread_t ballThread;
	pthread_create(&ballThread, &attr, &ballProgress, NULL);

	while(true)
	{
		char input = getchar();

		clearPlatform(&platformL);
		clearPlatform(&platformR);

		if (input == 'w')
			platformL.yPos -= (platformL.yPos - platformL.radius > 1) ? 1 : 0;
		else if (input == 's')
			platformL.yPos += (platformL.yPos + platformL.radius < height - 2) ? 1 : 0;
		else if (input == 'o')
			platformR.yPos -= (platformR.yPos - platformR.radius > 1) ? 1 : 0;
		else if (input == 'k')
			platformR.yPos += (platformR.yPos + platformR.radius < height - 2) ? 1 : 0;

		drawPlatform(&platformL);
		drawPlatform(&platformR);
	}

	return 0;
}

void moveBall()
{
	ball.xPos += ball.horDir;
	ball.yPos += ball.verDir;
}

void *ballProgress(void *arg)
{
	while(true)
	{
		if (detectCollision() == 1)
		{
			break;
		}
		clearBall();
		moveBall();
		drawBall();

		usleep(sleepTime * 1000);
	}
}

int detectCollision()
{
	int nextXPos = ball.xPos + ball.horDir;
	int nextYPos = ball.yPos + ball.verDir;

	// colision with left platform
	if (nextXPos == platformL.xPos && nextYPos <= (platformL.yPos + platformL.radius) && nextYPos >= (platformL.yPos - platformL.radius))
	{
		ball.horDir *= (-1);
		// ball.verDir *= (-1); ///
	}

	// colision with right platform
	if (nextXPos == platformR.xPos && nextYPos <= (platformR.yPos + platformR.radius) && nextYPos >= (platformR.yPos - platformR.radius))
	{
		ball.horDir *= (-1);
		// ball.verDir *= (-1); ///
	}

	// detect bottom and top
	if (nextYPos == 0 || nextYPos == height - 1) ///
	{
		ball.verDir *= (-1); ///
	}

	// detect left and right wall
	if (nextXPos < 1 || nextXPos > width - 2)
	{
		return 1;
	}
	return 0;
}

void drawBall()
{
	setCursorPos(ball.xPos, ball.yPos);
	printf("*\n");
	setCursorPos(0, height + 1);
}

void clearBall()
{
	setCursorPos(ball.xPos, ball.yPos);
	printf(" \n");
	setCursorPos(0, height + 1);
}

void drawPlatform(Platform *platform)
{
	for (int i = 0; i <= platform->radius; i++)
	{
		setCursorPos(platform->xPos, platform->yPos + i);
		printf("|\n");
		setCursorPos(platform->xPos, platform->yPos - i);
		printf("|\n");
	}
	setCursorPos(0, height + 1);
}

void clearPlatform(Platform *platform)
{
	for (int i = 0; i <= platform->radius; i++)
	{
		setCursorPos(platform->xPos, platform->yPos + i);
		printf(" \n");
		setCursorPos(platform->xPos, platform->yPos - i);
		printf(" \n");
	}
	setCursorPos(0, height + 1);
}

// get index in playground positional matrix
int getIndex(int x, int y)
{
	return (y * width) + x;
}

void drawFrame()
{
	clearScreen();
	for (int i = 0; i < width; i++)
	{
		setCursorPos(i, 0);
		printf("#\n");
		setCursorPos(i, height - 1);
		printf("#\n");
	}
	// horizontal borders
	for (int i = 0; i < height; i++)
	{
		setCursorPos(0, i);
		printf("#\n");
		setCursorPos(width - 1, i);
		printf("#\n");
	}
	printf("\n");
}

void setCursorPos(int xPos, int yPos)
{
	printf("\033[%d;%dH", yPos + 1, xPos + 1);
}

void clearScreen()
{
	printf("\033[H\033[J");
}

void initPlayground()
{
	ball.xPos = width / 2;
	ball.yPos = verticalCenter;
	ball.verDir = ((-1) + ((rand() % 2) * 2)); ///
	ball.horDir = ((-1) + ((rand() % 2) * 2));
	platformL.xPos = platformOffset;
	platformL.yPos = verticalCenter;
	platformL.radius = platformRadius;
	platformR.xPos = width - platformOffset - 1;
	platformR.yPos = verticalCenter;
	platformR.radius = platformRadius;
}
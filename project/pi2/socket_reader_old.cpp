#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <time.h>

int soc;
int read_can_port;

int open_port(const char *port)
{
    struct ifreq ifr;
    struct sockaddr_can addr;
    
    // open can socket
    soc = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if(soc < 0)
    {
        perror("Error while opening socket");
        return (-1);
    }

    addr.can_family = AF_CAN;  // address family CAN constant
    strcpy(ifr.ifr_name, port); // copy port name

    // get index numer of linux network interface
    if (ioctl(soc, SIOCGIFINDEX, &ifr) < 0)
    {
        perror("Cannot bind interface index");
        return (-1);
    }

    addr.can_ifindex = ifr.ifr_ifindex;
    fcntl(soc, F_SETFL, O_NONBLOCK); // set non blocking connection with CAN interface

    // bind socket
    if (bind(soc, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("Cannot bind socket");
        return (-1);
    }

    return 0;
}
int send_port(struct can_frame *frame)
{
    int retval;
    retval = write(soc, frame, sizeof(struct can_frame));
    if (retval != sizeof(struct can_frame))
    {
        return (-1);
    }
    else
    {
        return (0);
    }
}

/* this is just an example, run in a thread */
void read_port()
{
    struct can_frame frame_rd;
    int recvbytes = 0;
    read_can_port = 1;
    while(read_can_port)
    {
        struct timeval timeout = {1, 0};
        fd_set readSet;
        FD_ZERO(&readSet);
        FD_SET(soc, &readSet);
        if (select((soc + 1), &readSet, NULL, NULL, &timeout) >= 0)
        {
            if (!read_can_port)
            {
                break;
            }
            if (FD_ISSET(soc, &readSet))
            {
                recvbytes = read(soc, &frame_rd, sizeof(struct can_frame));
                if(recvbytes)
                {
                    printf("dlc = %d, data = ", frame_rd.can_dlc);
                    for (int c = 0; c < frame_rd.can_dlc; c++)
                        printf("%02x", frame_rd.data[c]);
                    printf("\n");
                }
            }
        }
    }
}

int close_port()
{
    close(soc);
    return 0;
}
int main(void)
{
    open_port("can0");

    struct can_frame msg;
    msg.can_id = 0xbaba;
    msg.can_dlc = 8;
    for (int i = 0; i < msg.can_dlc; i++)
        msg.data[i] = 0xba;

    send_port(&msg);
    read_port();
    return 0;
}
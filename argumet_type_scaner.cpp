#include <stdio.h>

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
	    printf("No parameters\n");
	}
	else
	{
	    printf("Program name: %s\n", argv[0]);
		int result;
	    for (int i = 1; i < argc; i++)
		{
			if (sscanf(argv[i], "%d", &result) != 0)
			{
			    printf("Arg is number: %d\n", result);
			}
			else
			{
			    printf("Arg is string: %s\n", argv[i]);
			}
		}
	}
}
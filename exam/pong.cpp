#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>

#define width 64
#define height 32
#define verticalCenter 16
#define platformRadius 2 // radius of platform
#define platformOffset 2 // distance between border and platform
#define sleepTime 150 // ball speed
#define waitingTime 3000 // time between games

struct Ball
{
	int xPos;
	int yPos;
	int horDir;
	int verDir;
};

struct Platform
{
	int xPos;
	int yPos;
	int radius;
};

struct PlatformAction
{
    struct Platform platform;
    char actionKey;
    char *actions;
};

void moveBall();
int getIndex(int, int);
void drawFrame();
void setCursorPos(int, int);
void clearScreen();
void initPlayground();
int detectCollision();
void drawBall();
void clearBall();
void *ballProgress(void*);
void drawPlatform(Platform*);
void clearPlatform(Platform*);
void *platformProgress(void*);
void drawScore(int, int);
void gameEnd();
void keyboardInterruptHandler(int);

int playground[width * height] = {0};
struct Ball ball;
struct Platform platformL, platformR;
int scoreL = 0;
int scoreR = 0;
int allowInput = 1;

int main(int argc, char *argv[])
{
    signal(SIGINT, keyboardInterruptHandler);

    if (!(argc >= 2 && strlen(argv[1]) == 4))
    {
        printf("Invalid cound of arguments or command set\n");
        exit(1);
    }
	srand((int) time(NULL));
	initPlayground();
	drawFrame();
	drawBall();
	usleep(waitingTime * 1000);

	pthread_attr_t attr;
	pthread_attr_init( &attr );
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	pthread_t ballThread;
	pthread_create(&ballThread, &attr, &ballProgress, NULL);

	while(true)
	{
		char input = getchar();

        if (allowInput)
        {
            struct PlatformAction *param = (struct PlatformAction*) malloc(sizeof(struct PlatformAction)); // MEM -
            param->actionKey = input;
            param->actions = argv[1];

            pthread_t platformTh;
            pthread_create(&platformTh, NULL, &platformProgress, param);
        }
	}

	return 0;
}

void moveBall()
{
	ball.xPos += ball.horDir;
	ball.yPos += ball.verDir;
}

void *ballProgress(void *arg)
{
	while(true)
	{
		if (detectCollision() == 1)
		{
            allowInput = 0;
			initPlayground();
            drawFrame();
            drawBall();
            drawScore(scoreL, scoreR);

            if (scoreL >= 10 || scoreR >= 10)
                gameEnd();

            usleep(waitingTime * 1000);
		}
		clearBall();
		moveBall();
		drawBall();

		usleep(sleepTime * 1000);
        allowInput = 1;
	}
}

void *platformProgress(void *arg)
{
    if (arg != NULL)
    {
        struct PlatformAction *param = (struct PlatformAction *)arg;

        char input = param->actionKey;

        clearPlatform(&platformL);
		clearPlatform(&platformR);

		if (input == param->actions[0])
			platformL.yPos -= (platformL.yPos - platformL.radius > 1) ? 1 : 0;
		else if (input == param->actions[1])
			platformL.yPos += (platformL.yPos + platformL.radius < height - 2) ? 1 : 0;
		else if (input == param->actions[2])
			platformR.yPos -= (platformR.yPos - platformR.radius > 1) ? 1 : 0;
		else if (input == param->actions[3])
			platformR.yPos += (platformR.yPos + platformR.radius < height - 2) ? 1 : 0;

		drawPlatform(&platformL);
		drawPlatform(&platformR);

        free(param);
    }
}

int detectCollision()
{
	int nextXPos = ball.xPos + ball.horDir;
	int nextYPos = ball.yPos + ball.verDir;

	// colision with left platform
	if (nextXPos == platformL.xPos && nextYPos <= (platformL.yPos + platformL.radius) && nextYPos >= (platformL.yPos - platformL.radius))
	{
		ball.horDir *= (-1);
        if (ball.verDir == 0)
        {
            ball.verDir = (nextYPos < platformL.yPos) ? (-1) : ball.verDir;
            ball.verDir = (nextYPos > platformL.yPos) ? 1 : ball.verDir;
        }
        else 
        {
            ball.verDir *= (nextYPos == platformL.yPos) ? 0 : (-1);
        }
	}

	// colision with right platform
	if (nextXPos == platformR.xPos && nextYPos <= (platformR.yPos + platformR.radius) && nextYPos >= (platformR.yPos - platformR.radius))
	{
		ball.horDir *= (-1);
		ball.verDir *= (-1);
        if (ball.verDir == 0)
        {
            ball.verDir = (nextYPos < platformR.yPos) ? (-1) : ball.verDir;
            ball.verDir = (nextYPos > platformR.yPos) ? 1 : ball.verDir;
        }
        else {
            ball.verDir *= (nextYPos == platformR.yPos) ? 0 : 1;
        }
	}

	// detect bottom and top
	if (nextYPos == 0 || nextYPos == (height - 1))
	{
		ball.verDir *= (-1);
	}

	// detect left
	if (nextXPos < 1)
	{
        scoreR++;
		return 1;
	}
    // detect right wall
    if (nextXPos > (width - 2))
    {
        scoreL++;
        return 1;
    }
	return 0;
}

void drawBall()
{
	setCursorPos(ball.xPos, ball.yPos);
	printf("*\n");
	setCursorPos(0, height + 1);
}

void clearBall()
{
	setCursorPos(ball.xPos, ball.yPos);
	printf(" \n");
	setCursorPos(0, height + 1);
}

void drawPlatform(Platform *platform)
{
	for (int i = 0; i <= platform->radius; i++)
	{
		setCursorPos(platform->xPos, platform->yPos + i);
		printf("|\n");
		setCursorPos(platform->xPos, platform->yPos - i);
		printf("|\n");
	}
	setCursorPos(0, height + 1);
}

void clearPlatform(Platform *platform)
{
	for (int i = 0; i <= platform->radius; i++)
	{
		setCursorPos(platform->xPos, platform->yPos + i);
		printf(" \n");
		setCursorPos(platform->xPos, platform->yPos - i);
		printf(" \n");
	}
	setCursorPos(0, height + 1);
}

// get index in playground positional matrix
int getIndex(int x, int y)
{
	return (y * width) + x;
}

void drawFrame()
{
	clearScreen();
	for (int i = 0; i < width; i++)
	{
		setCursorPos(i, 0);
		printf("#\n");
		setCursorPos(i, height - 1);
		printf("#\n");
	}
	// horizontal borders
	for (int i = 0; i < height; i++)
	{
		setCursorPos(0, i);
		printf("#\n");
		setCursorPos(width - 1, i);
		printf("#\n");
	}
    drawScore(scoreL, scoreR);
    drawPlatform(&platformL);
	drawPlatform(&platformR);

	printf("\n");
}

void setCursorPos(int xPos, int yPos)
{
	printf("\033[%d;%dH", yPos + 1, xPos + 1);
}

void clearScreen()
{
	printf("\033[H\033[J");
}

void drawScore(int left, int right)
{
    setCursorPos(10, height + 2);
    printf(">>> %d : %d <<<\n", left, right);
}

void initPlayground()
{
    int side = rand() % 2;
	ball.xPos = (side) ? platformOffset + 1 : width - platformOffset - 2;
	ball.yPos = verticalCenter;
	ball.verDir = 0;
	ball.horDir = ((-1) + (side * 2));
	platformL.xPos = platformOffset;
	platformL.yPos = verticalCenter;
	platformL.radius = platformRadius;
	platformR.xPos = width - platformOffset - 1;
	platformR.yPos = verticalCenter;
	platformR.radius = platformRadius;
}

void gameEnd()
{
    clearScreen();
    if (scoreL == scoreR)
        printf(">>>>> No one win the match <<<<<\n\n");
    else
        printf(">>>>> Player %s win the match <<<<<\n\n", ((scoreL > scoreR) ? "Left" : "Right"));
    exit(0);
}

void keyboardInterruptHandler(int signal)
{
    gameEnd();
}
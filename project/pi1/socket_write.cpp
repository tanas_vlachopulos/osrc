/**
 * CAN writer
 * Read temperature from Dallas D18B20 1-wire sensor and send it over can0 network interface
 * @author Tanasis Vlachopulos & Sarka Vavrova
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <inttypes.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdarg.h>

#define TIME_INTERVAL 5

int soc;

int readTemperature(const char *);
void sendToCan(unsigned char *, int);
void readFromCan();
int openPort(const char *port);
void closePort();
void intTo4Bytes(unsigned char *, unsigned int);
void intToBytes(unsigned char *, int, ...);

int main(void)
{
	if (openPort("can0") == 0)
	{
		while (true)
		{
			int temp = readTemperature("/sys/bus/w1/devices/28-01160052d2ff/w1_slave");
			printf("Temperature: %d \n", temp);

			unsigned char bytes[8];
			// intTo4Bytes(bytes, temp);
			intToBytes(bytes, 2, temp, time(NULL));

			sendToCan(bytes, 8);

			usleep(TIME_INTERVAL * 1000000);
		}

		closePort();
	}

	return 0;
}

/**
	Read temperature from Dallas DS18B20 sensor on 1-wire bus
	@param path Path to one wire device file
	@return temperature * 1000
*/
int readTemperature(const char *path)
{
	FILE *device = fopen(path, "r");

	int temperature = -1; // bad temperature
	char crcVar[5];

	if (device == NULL)
	{
		printf("Check connections %s\n", path);
		perror("\n");
	}

	if (device != NULL)
	{
		if (!ferror(device))
		{
			// parse status result from 1-wire bus
			// correct output -> 5e 01 4b 46 7f ff 0c 10 51 : crc=51 YES
			fscanf(device, "%*x %*x %*x %*x %*x %*x %*x %*x %*x : crc=%*x %s", crcVar);
			if (strncmp(crcVar, "YES", 3) == 0)
			{
				// parse temperature result from 1-wire bus
				//correct output -> 5e 01 4b 46 7f ff 0c 10 51 t=21875
				fscanf(device, "%*x %*x %*x %*x %*x %*x %*x %*x %*x t=%d", &temperature);
			}
			else
			{
				perror("Cannot read from thermometer\n");
			}
		}
		fclose(device);
	}

	return temperature;
}

/**
	Open CAN bus interface and bind socket
	@param port Name of CAN port
	@return 0 if success, -1 if error
*/
int openPort(const char *port)
{
	struct ifreq ifr;
	struct sockaddr_can addr;

	// open can socket
	soc = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (soc < 0)
	{
		perror("Error while opening socket\n");
		return (-1);
	}

	addr.can_family = AF_CAN;   // address family CAN constant
	strcpy(ifr.ifr_name, port); // copy port name

	// get index numer of linux network interface
	if (ioctl(soc, SIOCGIFINDEX, &ifr) < 0)
	{
		perror("Cannot bind interface index\n");
		return (-1);
	}

	addr.can_ifindex = ifr.ifr_ifindex;
	fcntl(soc, F_SETFL, O_NONBLOCK); // set non blocking connection with CAN interface

	// bind socket
	if (bind(soc, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("Cannot bind socket\n");
		return (-1);
	}

	return 0;
}

/**
	Close binded CAN port
*/
void closePort()
{
	close(soc);
}

/**
	Send bytes to CAN bus
	@param bytes Array of bytes
	@param len Length of bytes array
*/
void sendToCan(unsigned char *bytes, int len)
{
	int ret;
	int packets = len / 8;
	packets += (len % 8) == 0 ? 0 : 1;

	for (int packet = 0; packet < packets; packet++)
	{
		struct can_frame canMsg;
		canMsg.can_id = packet + 1;
		canMsg.can_dlc = ((packet + 1) * 8) > len ? (len % 8) : 8;

		for (int i = 0; i < canMsg.can_dlc; i++)
			canMsg.data[i] = bytes[(packet * 8) + i];

		ret = write(soc, &canMsg, sizeof(struct can_frame));
		if (ret != sizeof(struct can_frame))
		{
			perror("Error during sending packet\n");
		}
		else
		{
			printf("Send %d bytes\n", ret);
		}
	}
}

/**
	Convert int number to 4 byte array
	@param bytes 4 byte result array
	@param number Source number
*/
void intTo4Bytes(unsigned char *bytes, unsigned int number)
{
	bytes[0] = (char)(number);
	bytes[1] = (char)(number >> 8);
	bytes[2] = (char)(number >> 16);
	bytes[3] = (char)(number >> 24);
}

/**
	Convert variable count of integers to byte array
	@param bytes Byte array
	@param numCout Count of passed numbers
	@param ... Numbers to conversion
*/
void intToBytes(unsigned char *bytes, int numCount, ...)
{
	va_list args;
	va_start(args, numCount);

	for (int i = 0; i < numCount; i++)
	{
		unsigned int number = va_arg(args, unsigned int);
		bytes[i * 4] = (char)(number);
		bytes[(i * 4) + 1] = (char)(number >> 8);
		bytes[(i * 4) + 2] = (char)(number >> 16);
		bytes[(i * 4) + 3] = (char)(number >> 24);
	}

	va_end(args);
}

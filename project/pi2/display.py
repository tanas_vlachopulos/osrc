import sys
import Adafruit_SSD1306
import time
from PIL import Image, ImageDraw, ImageFont

def display(disp, temperature, tx_time=0, rx_time=0):
	# disp.begin()

	# clear display
	# disp.clear()
	# disp.display()

	w = disp.width
	h = disp.height
	padding = 2
	bottom = h - padding
	top = padding
	f_temperature = float(temperature) / 1000.0

	font_big = ImageFont.truetype('/home/pi/project/VCR_OSD_MONO_1.001.ttf', 21)
	font_small = ImageFont.truetype('/home/pi/project/pixelmix.ttf', 8)

	image = Image.new('1', (w, h))
	draw = ImageDraw.Draw(image)
	draw.rectangle((0, 0, w, h), outline=0, fill=0)  # clear image

	# draw temperature progress bar
	bar_width = int((f_temperature / 50.0) * w)
	# print(bar_width)
	draw.rectangle((0, 0, bar_width, 10), outline=255, fill=255)

	# draw text
	x = 6
	draw.text((x, top + 15), str.format('t:{} C', f_temperature), font=font_big, fill=255)
	draw.text((x, top + 40), str.format("Txt: {}", tx_time), font=font_small, fill=255)
	draw.text((x, top + 50), str.format("Rxt: {}", rx_time), font=font_small, fill=255)

	disp.image(image)
	disp.display()


if __name__ == "__main__":
	disp = Adafruit_SSD1306.SSD1306_128_64(rst=24)
	if len(sys.argv) == 4:
		display(disp, sys.argv[1], sys.argv[2], sys.argv[3])
	else:
		print("Invalit argument count")

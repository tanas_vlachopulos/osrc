#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEFAULT_SIZE 1000000
#define ALFABET_SIZE 62
#define DEFAULT_FILE_NAME "data.txt"

const char alfabet[ALFABET_SIZE + 1] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

int main(int argc, char *argv[]) {
    long fileSize = DEFAULT_SIZE;
    if (argc > 1) 
        fileSize = (sscanf(argv[1], "%ld", &fileSize) == 0) ? DEFAULT_SIZE : fileSize;

    FILE *f;
   	srand((int) time(NULL)); // init random generator

    f = fopen(DEFAULT_FILE_NAME, "w");

    for (int i = 0; i < fileSize; i++) {
        fprintf(f, "%c", alfabet[rand() % (ALFABET_SIZE)]);
    }

    fclose(f);
    return 0;
}
/**
*   CAN reader
*   Read temperature from can0 network interface and push it on SSD1306 display
*   @author Tanasis Vlachopulos & Sarka Vavrova
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <inttypes.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int soc;

void sendToCan(unsigned char *, int);
void readFromCan();
int openPort(const char *port);
void closePort();
unsigned int bytesToInt(unsigned char *, int, int);
void setDisplay(int, int, int);

int main(void)
{
    if (openPort("can0") == 0)
    {
        readFromCan();

        closePort();
    }

    return 0;
}

/**
	Open CAN bus interface and bind socket
	@param port Name of CAN port
	@return 0 if success, -1 if error
*/
int openPort(const char *port)
{
    struct ifreq ifr;
    struct sockaddr_can addr;

    // open can socket
    soc = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (soc < 0)
    {
        perror("Error while opening socket\n");
        return (-1);
    }

    addr.can_family = AF_CAN;   // address family CAN constant
    strcpy(ifr.ifr_name, port); // copy port name

    // get index numer of linux network interface
    if (ioctl(soc, SIOCGIFINDEX, &ifr) < 0)
    {
        perror("Cannot bind interface index\n");
        return (-1);
    }

    addr.can_ifindex = ifr.ifr_ifindex;
    fcntl(soc, F_SETFL, O_NONBLOCK); // set non blocking connection with CAN interface

    // bind socket
    if (bind(soc, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("Cannot bind socket\n");
        return (-1);
    }

    return 0;
}

/**
	Close binded CAN port
*/
void closePort()
{
    close(soc);
}

/**
	Send bytes to CAN bus
	@param bytes Array of bytes
	@param len Length of bytes array
*/
void sendToCan(unsigned char *bytes, int len)
{
    int ret;
    int packets = len / 8;
    packets += (len % 8) == 0 ? 0 : 1;

    for (int packet = 0; packet < packets; packet++)
    {
        struct can_frame canMsg;
        canMsg.can_id = packet + 1;
        canMsg.can_dlc = ((packet + 1) * 8) > len ? (len % 8) : 8;

        for (int i = 0; i < canMsg.can_dlc; i++)
            canMsg.data[i] = bytes[(packet * 8) + i];

        ret = write(soc, &canMsg, sizeof(struct can_frame));
        if (ret != sizeof(struct can_frame))
        {
            perror("Error during sending packet\n");
        }
        else
        {
            printf("Send %d bytes\n", ret);
        }
    }
}

/**
    Infinite reading from CAN bus
*/
void readFromCan()
{
    int bytesCount = 0;
    struct can_frame canMsg;

    while (true)
    {
        struct timeval timeout = {1, 0};
        fd_set fdSet;
        FD_ZERO(&fdSet);
        FD_SET(soc, &fdSet);

        // set periodic watchdog on CAN interface
        if (select((soc + 1), &fdSet, NULL, NULL, &timeout) >= 0)
        {
            if (FD_ISSET(soc, &fdSet))
            {
                // read data from CAN interface
                bytesCount = read(soc, &canMsg, sizeof(struct can_frame));
                if (bytesCount > 0)
                {
                    int temperature = bytesToInt(canMsg.data, canMsg.can_dlc, 0); // parse temperature
                    int rxTime = bytesToInt(canMsg.data, canMsg.can_dlc, 4); // parse transmission timestamp 

                    printf("Seq: %d, Length: %d, Temperature: %d, Time: %d\n",
                           canMsg.can_id,
                           canMsg.can_dlc,
                           temperature,
                           rxTime);

                    // send data to display
                    setDisplay(temperature, rxTime, (int)time(NULL));
                }
            }
        }
    }
}

/**
    Extract single integer from byte array
    @param bytes Bytes array containing integers
    @param len Length of byte array
    @param offset Start position for extraction
    @return Single integer extracted from byte array
*/
unsigned int bytesToInt(unsigned char *bytes, int len, int offset)
{
    unsigned int result = 0;
    if ((offset + 3) < len)
    {
        result |= bytes[offset + 3];
        result <<= 8;
        result |= bytes[offset + 2];
        result <<= 8;
        result |= bytes[offset + 1];
        result <<= 8;
        result |= bytes[offset];

        return result;
    }

    perror("Byte array is too short\n");
    return result;
}

/**
    Invoke python script to control display
    @param temperature Recieve temperature
    @param rxTime Recieve unix timestamp
    @param txTime Local unix timestamp
*/
void setDisplay(int temperature, int rxTime, int txTime)
{
    char cmd[71];
    sprintf(cmd, "python /home/pi/project/display.py %d %d %d", temperature, rxTime, txTime);
    system(cmd);
}
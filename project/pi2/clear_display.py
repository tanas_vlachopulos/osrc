import sys
import Adafruit_SSD1306
import time
from PIL import Image, ImageDraw, ImageFont


disp = Adafruit_SSD1306.SSD1306_128_64(rst=24)

disp.begin()

# clear display
disp.clear()
disp.display()

print("Cleaning display")
